//
//  ATAppDelegate.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATAppDelegate.h"
#import "ATMenuViewController.h"
#import "ATBarcodeViewController.h"



@interface ATAppDelegate ()

@end



@implementation ATAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [ATUtils setupApplicationStyle];
    [ATUtils setupNavigationBarAppearance];
    [ATUtils requestPermissionForLocalNotification];
    
    ATMenuViewController *menu_view_controller = [[ATMenuViewController alloc] initWithDefaultNibFile];
    ATBarcodeViewController *barcode_view_controller = [[ATBarcodeViewController alloc] initWithDefaultNibFile];
    UINavigationController *navigation_controller = [[UINavigationController alloc] initWithRootViewController:menu_view_controller];
    [navigation_controller pushViewController:barcode_view_controller animated:NO];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setRootViewController:navigation_controller];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
    [[ATDataAccess sharedInstance] beaconConnectedWithUUID:nil];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - UILocalNotification

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    NSLog(@"didReceiveLocalNotification: %@", notification);
}

@end
