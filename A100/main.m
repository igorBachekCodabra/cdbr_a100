//
//  main.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ATAppDelegate class]));
    }
}
