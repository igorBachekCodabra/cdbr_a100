//
//  ATDataAccess.h
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ATProductCategory.h"
#import "ATProduct.h"



@interface ATDataAccess : NSObject

+ (id)sharedInstance;
- (void)setup;
- (NSString *)userName;
- (NSNumber *)pointsCount;
- (void)beaconConnectedWithUUID:(NSString *)beaconUUID;


- (NSArray *)demoData;

@end
