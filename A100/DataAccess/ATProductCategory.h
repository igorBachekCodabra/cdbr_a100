//
//  ATProductCategory.h
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ATProductCategory : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *products;

@end
