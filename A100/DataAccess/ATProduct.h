//
//  ATProduct.h
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ATProduct : NSObject

@property (nonatomic, strong) NSString *productImageName;
@property (nonatomic, strong) NSNumber *productPoint;
@property (nonatomic, strong) NSString *productDescription;

@end
