//
//  ATDataAccess.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATDataAccess.h"



NSString *const kATUserName = @"Новик Игорь Иванович";
NSInteger const kATPointsCount = 120;



@implementation ATDataAccess


#pragma mark - Singleton realization and setup

+ (id)sharedInstance {
    
    static id sharedInstance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[super alloc] init];
    });
    
    
    return sharedInstance;
}


- (void)setup {
    
    
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Public methods

- (NSString *)userName {
    
    return kATUserName;
}


- (NSNumber *)pointsCount {
    
    return [NSNumber numberWithInteger:kATPointsCount];
}


- (void)beaconConnectedWithUUID:(NSString *)beaconUUID {
    
    UILocalNotification *local_notification = [[UILocalNotification alloc] init];
    
    NSDate *fire_date = [NSDate dateWithTimeIntervalSinceNow:10];
    [local_notification setFireDate:fire_date];
    [local_notification setTimeZone:[NSTimeZone defaultTimeZone]];
    [local_notification setAlertBody:@"У Вас накопилось 150 баллов, зайдите в каталог чтобы просмотреть перечень доступных товаров"];
    [local_notification setAlertAction:NSLocalizedString(@"notification_manager_view_details", nil)];
    [local_notification setSoundName:UILocalNotificationDefaultSoundName];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:local_notification];
    
}


#pragma mark - Demo methods

- (NSArray *)demoData {
    
    ATProduct *extinguisher = [[ATProduct alloc] init];
    [extinguisher setProductImageName:@"product_for_cars_extinguisher"];
    [extinguisher setProductDescription:@"Огнетушитель"];
    [extinguisher setProductPoint:@(338)];
    
    ATProduct *gloves = [[ATProduct alloc] init];
    [gloves setProductImageName:@"product_for_cars_gloves"];
    [gloves setProductDescription:@"Перчатки"];
    [gloves setProductPoint:@(13)];
    
    ATProduct *jack = [[ATProduct alloc] init];
    [jack setProductImageName:@"product_for_cars_jack"];
    [jack setProductDescription:@"Домкрат"];
    [jack setProductPoint:@(129)];
    
    ATProduct *medicine = [[ATProduct alloc] init];
    [medicine setProductImageName:@"product_for_cars_medicine"];
    [medicine setProductDescription:@"Аптечка"];
    [medicine setProductPoint:@(157)];
    
    ATProduct *wipes = [[ATProduct alloc] init];
    [wipes setProductImageName:@"product_for_cars_wipes"];
    [wipes setProductDescription:@"Салфетки влаж. д/рук А-100, РБ"];
    [wipes setProductPoint:@(8)];
    
    ATProductCategory *for_cars = [[ATProductCategory alloc] init];
    [for_cars setName:@"Автотовары"];
    [for_cars setProducts:@[medicine, jack, extinguisher, gloves, wipes]];
    
    
    
    ATProduct *bonaqua = [[ATProduct alloc] init];
    [bonaqua setProductImageName:@"product_drinks_bonaqua"];
    [bonaqua setProductDescription:@"Вода питьевая Бонаква 0,5л"];
    [bonaqua setProductPoint:@(13)];
    
    ATProduct *burn = [[ATProduct alloc] init];
    [burn setProductImageName:@"product_drinks_burn"];
    [burn setProductDescription:@"Burn, 250г. ж/б"];
    [burn setProductPoint:@(40)];
    
    ATProduct *tea = [[ATProduct alloc] init];
    [tea setProductImageName:@"product_drinks_tea"];
    [tea setProductDescription:@"Чай в ассортименте"];
    [tea setProductPoint:@(19)];
    
    ATProductCategory *drinks = [[ATProductCategory alloc] init];
    [drinks setName:@"Напитки"];
    [drinks setProducts:@[bonaqua, burn, tea]];
    
    
    
    ATProduct *hot_dog = [[ATProduct alloc] init];
    [hot_dog setProductImageName:@"product_eat_hot_dog"];
    [hot_dog setProductDescription:@"Хот-дог"];
    [hot_dog setProductPoint:@(40)];
    
    ATProduct *lays = [[ATProduct alloc] init];
    [lays setProductImageName:@"product_eat_lays"];
    [lays setProductDescription:@"Чипсы Лейз в ассорт 35г"];
    [lays setProductPoint:@(13)];
    
    ATProduct *orbit = [[ATProduct alloc] init];
    [orbit setProductImageName:@"product_eat_orbit"];
    [orbit setProductDescription:@"Жевательная резинка 'Орбит'"];
    [orbit setProductPoint:@(11)];
    
    ATProductCategory *eat = [[ATProductCategory alloc] init];
    [eat setName:@"Снеки"];
    [eat setProducts:@[hot_dog, lays, orbit]];
    
    
    
    NSArray *demo_data = @[for_cars, drinks, eat];
    return demo_data;
}


@end
