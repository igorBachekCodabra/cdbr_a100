//
//  ATBarcodeViewController.h
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATBaseViewController.h"



@interface ATBarcodeViewController : ATBaseViewController

@property (nonatomic, weak) IBOutlet UIButton *btnPoints;

@end
