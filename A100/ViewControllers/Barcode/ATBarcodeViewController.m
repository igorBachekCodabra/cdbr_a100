//
//  ATBarcodeViewController.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATBarcodeViewController.h"



@interface ATBarcodeViewController ()

@end



@implementation ATBarcodeViewController


#pragma mark - View Controller life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIBarButtonItem *right_item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon_tile"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(rightNavigationItemPressed:)];
    [self.navigationItem setRightBarButtonItem:right_item];
    [self.navigationItem setLeftBarButtonItem:nil];
    
    [self updateTopBarInformation];
    [self updateButtonPointsInformation];
}


- (void)rightNavigationItemPressed:(UIBarButtonItem *)barButtonItem {
    
    // Selector from superView
    [self showMenuScreen];
}


#pragma mark - Private methods

- (void)updateTopBarInformation {
    
    NSString *user_name = [[ATDataAccess sharedInstance] userName];
    [self setTitle:user_name];
}


- (void)updateButtonPointsInformation {
    
    NSNumber *points_count = [[ATDataAccess sharedInstance] pointsCount];
    NSString *points_count_prefix = NSLocalizedString(@"navigation_bar_points_count_prefix", nil);
    NSString *points_count_suffix = NSLocalizedString(@"navigation_bar_points_count_suffix", nil);
    
    NSString *title = [NSString stringWithFormat:@"%@ %@ %@", points_count_prefix, points_count, points_count_suffix];
    [self.btnPoints setTitle:title forState:UIControlStateNormal];
}


- (void)showMenuScreen {
    
    CATransition *transition = [CATransition animation];
    [transition setDuration:kAnimationDurationTransition];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transition setType:kCATransitionReveal];
    [transition setSubtype:kCATransitionFromTop];
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
    
    
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


@end
