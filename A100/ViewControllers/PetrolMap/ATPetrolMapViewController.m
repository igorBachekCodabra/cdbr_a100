//
//  ATPetrolMapViewController.m
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATPetrolMapViewController.h"



@interface ATPetrolMapViewController ()

@end



@implementation ATPetrolMapViewController


#pragma mark - View Controller life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setTitle:NSLocalizedString(@"petrol_map_navigation_bar_title", nil)];
    [self.navigationItem setHidesBackButton:NO];
}

@end
