//
//  ATBaseViewController.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATBaseViewController.h"



@interface ATBaseViewController ()

@end



@implementation ATBaseViewController


#pragma mark - View Controller life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    UIBarButtonItem *left_item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigation_bar_icon_back"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(leftNavigationItemPressed:)];
    [self.navigationItem setLeftBarButtonItem:left_item];
    [self.navigationItem setHidesBackButton:YES];

    
    UIColor *baground_color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"app_background_pattern_img"]];
    [self.view setBackgroundColor:baground_color];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleDefault;
}


- (void)rightNavigationItemPressed:(UIBarButtonItem *)barButtonItem {
    
    // Implemented in child ViewController
}


- (void)leftNavigationItemPressed:(UIBarButtonItem *)barButtonItem {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
