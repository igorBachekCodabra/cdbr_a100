//
//  ATFuelCostsViewController.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATFuelCostsViewController.h"



@interface ATFuelCostsViewController ()

@end



@implementation ATFuelCostsViewController


#pragma mark - View Controller life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setTitle:NSLocalizedString(@"fuel_costs_navigation_bar_title", nil)];
    [self.navigationItem setHidesBackButton:NO];
}


@end
