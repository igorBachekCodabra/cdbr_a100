//
//  ATOffersViewController.m
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATOffersViewController.h"



@interface ATOffersViewController ()

@end



@implementation ATOffersViewController


#pragma mark - View Controller life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setTitle:NSLocalizedString(@"offers_navigation_bar_title", nil)];
    [self.navigationItem setHidesBackButton:NO];
}


@end
