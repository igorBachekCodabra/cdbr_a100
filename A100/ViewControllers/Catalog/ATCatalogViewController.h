//
//  ATCatalogViewController.h
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATBaseViewController.h"



@interface ATCatalogViewController : ATBaseViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
