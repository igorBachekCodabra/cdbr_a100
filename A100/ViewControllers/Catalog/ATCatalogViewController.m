//
//  ATCatalogViewController.m
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATCatalogViewController.h"

#import "ATProductCell.h"



CGFloat const kTableViewSectionHeight = 40.0;



@interface ATCatalogViewController () <UITableViewDataSource, UITableViewDelegate/*, NSFetchedResultsControllerDelegate*/>

@property (nonatomic, strong) NSArray *productCategories;

@end



@implementation ATCatalogViewController


#pragma mark - View Controller life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setTitle:NSLocalizedString(@"catalog_navigation_bar_title", nil)];
    [self.navigationItem setHidesBackButton:NO];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self setupTableView];
    self.productCategories = [[ATDataAccess sharedInstance] demoData];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.productCategories.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    ATProductCategory *product_category = self.productCategories[section];
    return [product_category.products count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ATProductCell *product_cell = (ATProductCell *)[tableView dequeueReusableCellWithIdentifier:[ATProductCell identifier]];
    if (!product_cell) product_cell = [ATProductCell customViewWithPresetNibFile];
    
    [self configureCell:product_cell atIndexPath:indexPath];
    
    return product_cell;
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath*)indexPath {
    
    ATProductCell *product_cell = (ATProductCell *)cell;
    
    ATProductCategory *product_category = self.productCategories[indexPath.section];
    ATProduct *product = product_category.products[indexPath.row];
    
    
    [product_cell.ivProductImage setImage:[UIImage imageNamed:product.productImageName]];
    [product_cell.lProductPoint setText:[product.productPoint stringValue]];
    [product_cell.lProductDescription setText:product.productDescription];
    
    
    NSNumber *user_point_count = [[ATDataAccess sharedInstance] pointsCount];
    
    if (user_point_count.integerValue >= product.productPoint.integerValue)
        [product_cell setProductAvalible:YES];
    else
        [product_cell setProductAvalible:NO];
}


#pragma mark - UITableViewDelegate


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return kTableViewSectionHeight;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [ATProductCell height];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, kTableViewSectionHeight)];
    [title setBackgroundColor:[UIColor whiteColor]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTextColor:[UIColor orangeColor]];
    [title setFont:[UIFont boldSystemFontOfSize:15]];
    
    ATProductCategory *product_category = self.productCategories[section];
    [title setText:product_category.name];
    
    
    return title;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    [UIView animateWithDuration:kAnimationDurationNormal animations:^{
        
        [self.searchDisplayController setActive:NO animated:NO];
        
    } completion:^(BOOL finished) {
        
        
    }];
}


#pragma mark - Private methods

- (void)setupTableView {
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    
    NSNumber *points_count = [[ATDataAccess sharedInstance] pointsCount];
    NSString *points_count_prefix = NSLocalizedString(@"navigation_bar_points_count_prefix", nil);
    NSString *points_count_suffix = NSLocalizedString(@"navigation_bar_points_count_suffix", nil);
    
    NSString *title = [NSString stringWithFormat:@"%@ %@ %@", points_count_prefix, points_count, points_count_suffix];
    [self setTitle:title];
}


@end
