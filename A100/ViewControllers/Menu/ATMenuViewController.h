//
//  ATMenuViewController.h
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATBaseViewController.h"



@interface ATMenuViewController : ATBaseViewController

@property (nonatomic, weak) IBOutlet UIButton *btnMenuCatalog;
@property (nonatomic, weak) IBOutlet UIButton *btnMenuOffer;
@property (nonatomic, weak) IBOutlet UIButton *btnMenuFuelCosts;
@property (nonatomic, weak) IBOutlet UIButton *btnMenuMap;
@property (nonatomic, weak) IBOutlet UIButton *btnMenuFavorite;
@property (nonatomic, weak) IBOutlet UIButton *btnMenuServices;

@end
