//
//  ATMenuViewController.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATMenuViewController.h"

#import "ATBarcodeViewController.h"
#import "ATCatalogViewController.h"
#import "ATOffersViewController.h"
#import "ATFuelCostsViewController.h"
#import "ATPetrolMapViewController.h"
#import "ATFavoriteViewController.h"
#import "ATServicesViewController.h"



@interface ATMenuViewController ()

- (IBAction)buttonMenuCatalogPressed:(id)sender;
- (IBAction)buttonMenuOfferPressed:(id)sender;
- (IBAction)buttonMenuFuelCostsPressed:(id)sender;
- (IBAction)buttonMenuPetrolMapPressed:(id)sender;
- (IBAction)buttonMenuFavoritePressed:(id)sender;
- (IBAction)buttonMenuServicesPressed:(id)sender;

@end



@implementation ATMenuViewController


#pragma mark - View Controller life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIBarButtonItem *right_item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon_barcode"]
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(rightNavigationItemPressed:)];
    [self.navigationItem setRightBarButtonItem:right_item];
    [self.navigationItem setLeftBarButtonItem:nil];
    
    [self updateTopBarInformation];
}


- (void)rightNavigationItemPressed:(UIBarButtonItem *)barButtonItem {
    
    // Selector from superView
    [self showBarcodeScreen];
}


#pragma mark - IBAction

- (IBAction)buttonMenuCatalogPressed:(id)sender {
    
    ATCatalogViewController *catalog_costs_view_controller = [[ATCatalogViewController alloc] initWithDefaultNibFile];
    [self.navigationController pushViewController:catalog_costs_view_controller animated:YES];
}


- (IBAction)buttonMenuOfferPressed:(id)sender {
    
    ATOffersViewController *offer_costs_view_controller = [[ATOffersViewController alloc] initWithDefaultNibFile];
    [self.navigationController pushViewController:offer_costs_view_controller animated:YES];
}


- (IBAction)buttonMenuFuelCostsPressed:(id)sender {
    
    ATFuelCostsViewController *fuel_costs_view_controller = [[ATFuelCostsViewController alloc] initWithDefaultNibFile];
    [self.navigationController pushViewController:fuel_costs_view_controller animated:YES];
}


- (IBAction)buttonMenuPetrolMapPressed:(id)sender {
    
    ATPetrolMapViewController *petrol_map_view_controller = [[ATPetrolMapViewController alloc] initWithDefaultNibFile];
    [self.navigationController pushViewController:petrol_map_view_controller animated:YES];
}


- (IBAction)buttonMenuFavoritePressed:(id)sender {
    
    ATFavoriteViewController *favorite_view_controller = [[ATFavoriteViewController alloc] initWithDefaultNibFile];
    [self.navigationController pushViewController:favorite_view_controller animated:YES];
}


- (IBAction)buttonMenuServicesPressed:(id)sender {
    
    ATServicesViewController *services_costs_view_controller = [[ATServicesViewController alloc] initWithDefaultNibFile];
    [self.navigationController pushViewController:services_costs_view_controller animated:YES];
}


#pragma mark - Private methods

- (void)updateTopBarInformation {
    
    NSNumber *points_count = [[ATDataAccess sharedInstance] pointsCount];
    NSString *points_count_prefix = NSLocalizedString(@"navigation_bar_points_count_prefix", nil);
    NSString *points_count_suffix = NSLocalizedString(@"navigation_bar_points_count_suffix", nil);
    
    NSString *title = [NSString stringWithFormat:@"%@ %@ %@", points_count_prefix, points_count, points_count_suffix];
    [self setTitle:title];
}


#pragma mark - Transition

- (void)showBarcodeScreen {
    
//    ATBarcodeViewController *barcode_view_controller = [[ATBarcodeViewController alloc] initWithDefaultNibFile];
//    [self.navigationController presentViewController:barcode_view_controller animated:YES completion:nil];
    
    CATransition *transition = [CATransition animation];
    [transition setDuration:kAnimationDurationTransition];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transition setType:kCATransitionReveal];
    [transition setSubtype:kCATransitionFromBottom];
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    
    ATBarcodeViewController *barcode_view_controller = [[ATBarcodeViewController alloc] initWithDefaultNibFile];
    [self.navigationController pushViewController:barcode_view_controller animated:NO];
}


@end
