//
//  ATServicesViewController.m
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATServicesViewController.h"



@interface ATServicesViewController ()

@end



@implementation ATServicesViewController


#pragma mark - View Controller life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setTitle:NSLocalizedString(@"services_navigation_bar_title", nil)];
    [self.navigationItem setHidesBackButton:NO];
}


@end
