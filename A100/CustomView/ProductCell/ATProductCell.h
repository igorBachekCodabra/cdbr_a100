//
//  ATProductCell.h
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import <UIKit/UIKit.h>



#define kATProductCellHeight 126
#define kATProductCellAvailableColor [UIColor colorWithRed:226.0/255.0 green:160.0/255.0 blue:5.0/255.0 alpha:1.0]
#define kATProductCellInaccessibleColor [UIColor colorWithRed:204.0/255.0 green:88.0/255.0 blue:82.0/255.0 alpha:1.0]



@interface ATProductCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ivProductImage;
@property (nonatomic, weak) IBOutlet UILabel *lProductPoint;
@property (nonatomic, weak) IBOutlet UILabel *lProductDescription;
@property (nonatomic, weak) IBOutlet UIView *vProductPointContainer;
@property (nonatomic, weak) IBOutlet UIView *vProductInfoContainer;


+ (ATProductCell *)customViewWithPresetNibFile;
+ (NSString *)identifier;
+ (CGFloat)height;

- (void)setProductAvalible:(BOOL)avalible;

@end
