//
//  ATProductCell.m
//  A100
//
//  Created by Igor Bachek on 1/28/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATProductCell.h"



@implementation ATProductCell


+ (ATProductCell *)customViewWithPresetNibFile {
    
    ATProductCell *new_cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:Nil] firstObject];
    
    if (new_cell)
        [new_cell setup];
    
    return new_cell;
}


- (void)awakeFromNib {
    
    [super awakeFromNib];
    [self setup];
}


+ (NSString *)identifier {
    
    return NSStringFromClass(self.class);
}


+ (CGFloat)height {
    
    return kATProductCellHeight;
}


- (void)setup {
    
    [self setBackgroundColor:[UIColor whiteColor]];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    
    [self.vProductInfoContainer.layer setBorderWidth:1.0];
    [self.vProductInfoContainer.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.vProductInfoContainer.layer setCornerRadius:5.0];
    [self.vProductInfoContainer.layer setMasksToBounds:YES];
}


- (void)setProductAvalible:(BOOL)avalible {
    
    if (avalible)
        [self.vProductPointContainer setBackgroundColor:kATProductCellAvailableColor];
    else
        [self.vProductPointContainer setBackgroundColor:kATProductCellInaccessibleColor];
}


@end
