//
//  ATConstants.h
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ATConstants : NSObject


#pragma mark - Global

extern CGFloat const kStatusBarHeight;


#pragma mark - Animations

extern NSTimeInterval const kAnimationDurationTransition;
extern NSTimeInterval const kAnimationDurationNormal;


@end
