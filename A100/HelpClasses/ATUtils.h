//
//  ATUtils.h
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ATUtils : NSObject

+ (void)setupApplicationStyle;
+ (void)setupNavigationBarAppearance;
+ (void)requestPermissionForLocalNotification;

@end
