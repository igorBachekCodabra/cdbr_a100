//
//  ATUtils.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATUtils.h"



@implementation ATUtils

+ (void)setupApplicationStyle {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


+ (void)setupNavigationBarAppearance {
    
    
    UIImage *navigation_bar_background = [[UIImage imageNamed:@"navigation_bar_background"] resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    
    [[UINavigationBar appearance] setBackgroundImage:navigation_bar_background forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
}


+ (void)requestPermissionForLocalNotification {
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIApplication *application = [UIApplication sharedApplication];
        UIUserNotificationSettings *user_notification_settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound
                                                                                                   categories:nil];
        [application registerUserNotificationSettings:user_notification_settings];
    }
}

@end
