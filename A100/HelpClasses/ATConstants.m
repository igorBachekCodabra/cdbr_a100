//
//  ATConstants.m
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import "ATConstants.h"



@implementation ATConstants


#pragma mark - Global

CGFloat const kStatusBarHeight                = 20.0;


#pragma mark - Animations

NSTimeInterval const kAnimationDurationTransition = 0.35;
NSTimeInterval const kAnimationDurationNormal     = 0.35;

@end
