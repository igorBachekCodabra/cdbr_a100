//
//  ATAppDelegate.h
//  A100
//
//  Created by Igor Bachek on 1/27/15.
//  Copyright (c) 2015 Codabra Soft. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ATAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

