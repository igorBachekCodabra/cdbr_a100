//
//  UIViewController+Init.m
//  ZleepAlarm
//
//  Created by Mobexs on 6/27/14.
//  Copyright (c) 2014 Mobexs Developer. All rights reserved.
//

#import "UIViewController+Init.h"



@implementation UIViewController (Init)

- (id)initWithDefaultNibFile
{
    self = [self initWithNibName:NSStringFromClass([self class]) bundle:nil];
    return self;
}

@end
