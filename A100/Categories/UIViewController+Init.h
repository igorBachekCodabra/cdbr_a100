//
//  UIViewController+Init.h
//  ZleepAlarm
//
//  Created by Mobexs on 6/27/14.
//  Copyright (c) 2014 Mobexs Developer. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface UIViewController (Init)

- (id)initWithDefaultNibFile;

@end
